// Create a game of (rock paper and scissors)
// the user will input "rock" || "paper" || "scissors"
// rock paper and scissors are the only one accepted in input
// the compuer opponent will generate a random selection.
// if the computer beaten the user it will add one point to the computer
// if the user won the round one point will be fiven to the user

// every round there will be recap of scores.
// the first one who reach 5 points won the game

console.log(`To play rock-paper-scissors, enter "game()"`)

let turns = 0;
let playerTwoWin = 0;
let playerOneWin = 0;
let option = null;

let game = function() {
    let choices = ["Rock", "Paper", "Scissors"];
    let random = Math.floor(Math.random() * 3);
    
    if (turns == 0) {
        alert("Welcome to Rock-Paper-Scissors Game!");
        alert("The first who reaches 5 points wins!");
        
        optionArray = ["Human vs Human", "Human vs Computer"]

        option = prompt('Enter 1 for Human vs Human mode, enter 2 for Human vs Computer mode.')

        if (option == 1) {
            console.log(`Game is set to: ${optionArray[0]} Mode.`)
        } else if (option == 2) {
            console.log(`Game is set to: ${optionArray[1]} Mode.`)
        } else {
            return `Invalid input from user. Enter game() to try again.`
        }

    }
    
    this.turnCount = turns.length;
    this.playerOne = prompt(`Player 1 -- rock, paper, or scissors?`);
    
    if (option == 1) {
        this.playerTwo = prompt(`Player 2 -- rock, paper, or scissors?`);
    } if (option == 2) {
        this.playerTwo = choices[random];
    }
    
    if (this.playerOne.toLowerCase() != "rock" && this.playerOne.toLowerCase() != "paper" && this.playerOne.toLowerCase() != "scissors") {
        return `Rock, paper or scissors. Input of Player 1 is invalid.`
    } else {
        if (this.playerOne.toLowerCase() == "rock") {
            if (this.playerTwo.toLowerCase() == "rock") {
                console.log(`Player 1 chose ${playerOne.toLowerCase()}!`)
                console.log(`Player 2 chose ${playerTwo.toLowerCase()}!`)
                console.log(`Stalemate...`)
                turns++;
            } else if (this.playerTwo.toLowerCase() == "paper") {
                console.log(`Player 1 chose ${playerOne.toLowerCase()}!`)
                console.log(`Player 2 chose ${playerTwo.toLowerCase()}!`)
                console.log(`Player 2 wins!`)
                turns++;
                playerTwoWin++;
            } else if (this.playerTwo.toLowerCase() == "scissors") {
                console.log(`Player 1 chose ${playerOne.toLowerCase()}!`)
                console.log(`Player 2 chose ${playerTwo.toLowerCase()}!`)
                console.log(`Player 1 wins!`)
                turns++;
                playerOneWin++;
            }
        } else if (this.playerOne.toLowerCase() == "paper") {
            if (this.playerTwo.toLowerCase() == "paper") {
                console.log(`Player 1 chose ${playerOne.toLowerCase()}!`)
                console.log(`Player 2 chose ${playerTwo.toLowerCase()}!`)
                console.log(`Stalemate...`)
                turns++;
            } else if (this.playerTwo.toLowerCase() == "scissors") {
                console.log(`Player 1 chose ${playerOne.toLowerCase()}!`)
                console.log(`Player 2 chose ${playerTwo.toLowerCase()}!`)
                console.log(`Player 2 wins!`)
                turns++;
                playerTwoWin++;
            } else if (this.playerTwo.toLowerCase() == "rock") {
                console.log(`Player 1 chose ${playerOne.toLowerCase()}!`)
                console.log(`Player 2 chose ${playerTwo.toLowerCase()}!`)
                console.log(`Player 1 wins!`)
                turns++;
                playerOneWin++;
            }
        } else if (this.playerOne.toLowerCase() == "scissors") {
            if (this.playerTwo.toLowerCase() == "scissors") {
                console.log(`Player 1 chose ${playerOne.toLowerCase()}!`)
                console.log(`Player 2 chose ${playerTwo.toLowerCase()}!`)
                console.log(`Stalemate...`)
                turns++;
            } else if (this.playerTwo.toLowerCase() == "rock") {
                console.log(`Player 1 chose ${playerOne.toLowerCase()}!`)
                console.log(`Player 2 chose ${playerTwo.toLowerCase()}!`)
                console.log(`Player 2 wins!`)
                turns++;
                playerTwoWin++;
            } else if (this.playerTwo.toLowerCase() == "paper") {
                console.log(`Player 1 chose ${playerOne.toLowerCase()}!`)
                console.log(`Player 2 chose ${playerTwo.toLowerCase()}!`)
                console.log(`Player 1 wins!`)
                turns++;
                playerOneWin++;
            }
        } else {
            return `Rock, paper or scissors. Input of Player 2 is invalid.`
        }

        if (playerTwoWin == 5) {
            console.log(`Player 1 score: ${playerOneWin}. Player 2 score: ${playerTwoWin}. Total turns: ${turns}`)
            turns = 0;
            playerOneWin = 0;
            playerTwoWin = 0;
            return `GAME OVER. Player 2 wins.`
        } else if (playerOneWin == 5) {
            console.log(`Player 1 score: ${playerOneWin}. Player 2 score: ${playerTwoWin}. Total turns: ${turns}`)
            turns = 0;
            playerOneWin = 0;
            playerTwoWin = 0;
            return `GAME OVER. Player 1 wins. Congratulations!`
        }else {
            return `Player 1 score: ${playerOneWin}. Player 2 score: ${playerTwoWin}. Total turns: ${turns}`
        }
    }
}
